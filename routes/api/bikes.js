const express = require('express');

const router = express.Router();
const bike = require('../../controllers/api/bike');

router.get('/', bike.index);
router.post('/store', bike.store);
router.put('/update', bike.update);
router.delete('/delete', bike.delete);

module.exports = router;