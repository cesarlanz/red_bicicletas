const express = require('express');

const router = express.Router();
const bike = require('../controllers/bike');

router.get('/', bike.index);
router.get('/:id/show', bike.show);
router.get('/create', bike.create);
router.post('/store', bike.store);
router.get('/:id/edit', bike.edit);
router.post('/:id/update', bike.update);
router.post('/:id/delete', bike.delete);

module.exports = router;