var map = L.map('mapid').setView([-34.603629, -58.381556], 13);

//TILELAYER
L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors',
}).addTo(map);


// L.marker([-34.603629, -58.381556]).addTo(map);
// L.marker([-34.593629, -58.381556]).addTo(map);
// L.marker([-34.603629, -58.371556]).addTo(map);


$.ajax({
    dataType: "json",
    url: "api/bikes",
    success: function(result) {
        result.bikes.forEach(function(bike) {
            L.marker(bike.location, {title: bike.id}).addTo(map);
        });
    }
});