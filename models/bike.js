const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const bikeSchema = new Schema({
    code: Number,
    color: String,
    model: String,
    location: {
        type: [Number],
        index: {
            type: '2dsphere',
            sparse: true
        }
    }
});

bikeSchema.statics.createInstance = function(code, color, model, location) {
    return new this({
        code,
        color,
        model,
        location
    });
}

bikeSchema.methods.toString = function() {
    return `id: ${this.id} | color: ${this.color}`;
}

bikeSchema.statics.allBikes = function(cb) {
    return this.find({}, cb);
}

bikeSchema.statics.add = function(bike, cb) {
    this.create(bike, cb);
}

bikeSchema.statics.findByCode = function(code, cb) {
    return this.findOne({ code }, cb);
}

bikeSchema.statics.updateByCode = function(code, bike, cb) {
    return this.updateOne({ code }, bike, cb);
}

bikeSchema.statics.removeByCode = function(code, cb) {
    return this.deleteOne({ code }, cb);
}


const Bike = mongoose.model('Bike', bikeSchema);

// Bike.createInstance(1, 'rojo', 'urbana', [-34.6012424, -58.3861497]).save();
// Bike.createInstance(2, 'verde', 'montaña', [-34.6012424, -58.3861497]).save();
// Bike.allBikes(function (err, kittens) {
//     if (err) return console.error(err);
//     console.log(kittens);
//   });

module.exports = Bike;
