const mongoose = require('mongoose');
const Bike = require('../../models/bike');
require('../../bin/www');


describe('Testing Bikes', () => {
    
    // beforeAll((done) => {
    //     mongoose.connect('mongodb://localhost/testdb', {useNewUrlParser: true, useUnifiedTopology: true});
    //     const db = mongoose.connection;
    //     db.on('error', console.error.bind(console, 'connection error:'));
    //     db.once('open', function() {
    //         console.log('We are connected to test database!');
    //         done();
    //     });
    // });

    beforeEach((done) => {
        Bike.deleteMany({}, (err, success) => {
            if (err) console.log(err);
            done();
        });
    });

    afterEach((done) => {
        Bike.deleteMany({}, (err, success) => {
            if (err) console.log(err);
            done();
        });
    });

    describe('Bike.createInstance', () => {
        it('should create bike instance', () => {
            const bike = Bike.createInstance(1, 'rojo', 'urbana', [-34.6012424, -58.3861497]);
            expect(bike.code).toBe(1);
            expect(bike.color).toBe('rojo');
            expect(bike.model).toBe('urbana');
            expect(bike.location[0]).toBe(-34.6012424);
            expect(bike.location[1]).toBe(-58.3861497);
        });
    });

    describe('Bike.allBikes', () => {
        it('should begin empty', (done) => {
            Bike.allBikes((err, bikes) => {
                expect(bikes.length).toBe(0);
                done();
            });
        });
    });

    describe('Bike.add', () => {
        it('should add one bike', (done) => {
            const bike = new Bike({ code: 1, color: 'rojo', modelo: 'urbana' });
            Bike.add(bike, (err, newBike) => {
                if (err) console.log(err);
                Bike.allBikes((err, bikes) => {
                    expect(bikes.length).toBe(1);
                    expect(bikes[0].code).toBe(bike.code);
                    done();
                });
            });
        });
    });

    describe('Bike.findByCode', () => {
        it('should return the bike code 1', (done) => {
            Bike.allBikes((err, bikes) => {
                expect(bikes.length).toBe(0);
                const bike1 = new Bike({ code: 1, color: 'rojo', model: 'urbana' });
                Bike.add(bike1, (err, newBike1) => {
                    if (err) console.log(err);
                    const bike2 = new Bike({ code: 2, color: 'verde', model: 'montaña' });
                    Bike.add(bike2, (err, newBike2) => {
                        if (err) console.log(err);
                        Bike.findByCode(1, (err, bike) => {
                            expect(bike.code).toBe(bike1.code);
                            expect(bike.color).toBe(bike1.color);
                            expect(bike.model).toBe(bike1.model);
                            done();
                        });
                    });
                });
            });
        });
    });

});
