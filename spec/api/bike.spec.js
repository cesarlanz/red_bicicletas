const mongoose = require('mongoose');
const request = require('request');
const Bike = require('../../models/bike');
require('../../bin/www');

describe('Bikes API', () => {

    // beforeAll((done) => {
    //     mongoose.connect('mongodb://localhost/testdb', {useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true});
    //     const db = mongoose.connection;
    //     db.on('error', console.error.bind(console, 'connection error:'));
    //     db.once('open', function() {
    //         console.log('We are connected to test database!');
    //         done();
    //     });
    // });

    beforeEach((done) => {
        Bike.deleteMany({}, (err, success) => {
            if (err) console.log(err);
            done();
        });
    });

    afterEach((done) => {
        Bike.deleteMany({}, (err, success) => {
            if (err) console.log(err);
            done();
        });
    });

    describe('GET BIKES /', () => {
        it('STATUS 200', (done) => {
            request.get('http://localhost:3000/api/bikes', (err, res, body) => {
                const result = JSON.parse(body);
                expect(res.statusCode).toBe(200);
                expect(result.bikes.length).toBe(0);
                done();
            });
        });
    });

    describe('POST BIKES /create', () => {
        it('STATUS 201', (done) => {
            const headers = {'Content-Type': 'application/json'};
            const aBike = `{
                "code": 1,
                "color": "rojo",
                "model": "urbana",
                "lat": -34.6012424,
                "lng": -58.3861497
            }`;
            request.post({
                headers, 
                url: 'http://localhost:3000/api/bikes/store',
                body: aBike
            }, (err, res, body) => {
                expect(res.statusCode).toBe(201);
                const bike = JSON.parse(body).bike;
                expect(bike.color).toBe('rojo');
                expect(bike.model).toBe('urbana');
                expect(bike.location[0]).toBe(-34.6012424);
                expect(bike.location[1]).toBe(-58.3861497);
                done();
            });
        });
    });

    describe('PUT BIKES /update', () => {
        it('STATUS 200', (done) => {
            const bike = Bike.createInstance(1, 'rojo', 'urbana', [-34.6012424, -58.3861497]);
            Bike.add(bike, (err, newBike) => {
                const headers = {'Content-Type': 'application/json'};
                const json = `{
                    "code": 1,
                    "color": "verde",
                    "model": "montaña",
                    "lat": -30,
                    "lng": -50
                }`;
                request.put({
                    headers, 
                    url: 'http://localhost:3000/api/bikes/update',
                    body: json
                }, (err, res, body) => {
                    expect(res.statusCode).toBe(200);
                    const bike = JSON.parse(body).bike;
                    expect(bike.color).toBe('verde');
                    done();
                });
            });
        });
    });

    describe('DELETE BIKES /delete', () => {
        it('STATUS 204', (done) => {
            const bike = Bike.createInstance(1, 'rojo', 'urbana', [-34.6012424, -58.3861497]);
            Bike.add(bike, (err, newBike) => {
                const headers = {'Content-Type': 'application/json'};
                const json = `{
                    "code": 1
                }`;
                request.delete({
                    headers, 
                    url: 'http://localhost:3000/api/bikes/delete',
                    body: json
                }, (err, res, body) => {
                    expect(res.statusCode).toBe(204);
                    done();
                });
            });
        });
    });

});
