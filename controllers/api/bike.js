const Bike = require('../../models/bike');

exports.index = (req, res) => {
    Bike.allBikes((err, bikes) => {
        res.json({
            bikes
        });
    });
}

exports.store = (req, res) => {
    const { body: { code, color, model, lat, lng } } = req;
    const location = [lat, lng];
    const bike = { code, color, model, location };
    Bike.add(bike, (err, newBike) => {
        res.status(201).json({
            bike
        });
    });
}

exports.update = (req, res) => {
    const { body: { code, color, model, lat, lng } } = req;
    const location = [lat, lng];
    const bike = { code, color, model, lat, lng };
    Bike.updateByCode(code, bike, (err, newBike) => {
        res.json({
            bike
        });
    });
}

exports.delete = (req, res) => {
    const { body: { code } } = req;
    Bike.removeByCode(code, (err, bike) => {
        res.status(204).send();
    });
}