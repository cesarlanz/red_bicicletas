const Bike = require('../models/bike');

exports.index = (req, res) => {
    res.render('bikes/index', {bikes: Bike.allBikes});
}

exports.show = (req, res) => {
    const { params } = req;
    const { id } = params;
    const bike = Bike.findById(id);
    res.render('bikes/show', { bike });
}

exports.create = (req, res) => {
    res.render('bikes/create');
}

exports.store = (req, res) => {
    const { body } = req;
    const { id, color, model, lat, lng } = body;
    const location = [lat, lng];
    Bike.add({ id, color, model, location });
    res.redirect('/bikes');
}

exports.edit = (req, res) => {
    const { params } = req;
    const { id } = params;
    const bike = Bike.findById(id);
    res.render('bikes/edit', { bike });
}

exports.update = (req, res) => {
    const { params, body } = req;
    const { id, color, model, lat, lng } = body;
    const location = [lat, lng];
    const bike = Bike.findById(params.id);
    bike.id = id;
    bike.color = color;
    bike.model = model;
    bike.location = location;    
    res.redirect('/bikes');
}

exports.delete = (req, res) => {
    const { body } = req;
    const { id } = body;
    Bike.removeById(id);
    res.redirect('/bikes');
}
